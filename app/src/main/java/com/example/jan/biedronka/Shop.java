package com.example.jan.biedronka;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by jan on 06.08.15.
 */
public class Shop {
    private Long Id;
    private float Lat,Lng;
    private String Address;
    private String Hours;

    public Shop(Long long1,float set_lat, float set_lng, String set_address,String set_hours) {
        this.Id = long1;
        this.Lat = set_lat;
        this.Lng = set_lng;
        this.Address = set_address;
        this.Hours = set_hours;
    }
    float getLat(){
        return this.Lat;
    }
    float getId(){
        return this.Id;
    }
    float getLng(){
        return this.Lng;
    }
    LatLng getPosition()
    {
        return new LatLng(this.Lng,this.Lat);
    }
    String getAddress(){
        return this.Address;
    }
    String getHours(){
        return this.Hours;
    }


}
