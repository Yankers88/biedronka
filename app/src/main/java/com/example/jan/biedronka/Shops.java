package com.example.jan.biedronka;

/**
 * Created by jan on 06.08.15.
 */



import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

@SuppressWarnings("deprecation")
public class Shops
{
    String cache = Environment.getExternalStorageDirectory()+"/.config.txt";
    String surl;
    Shop[] biedronkas;
    float min_lat;
    float min_lng;
    float max_lat;
    float max_lng;
    GoogleMap mMapa;
    Context con;
    public Shops(GoogleMap mMap,Context context)
    {
        mMapa = mMap;
        con = context;
        min_lat=500;
        min_lng=500;
        max_lat=-500;
        max_lng=-500;
        surl = "https://api.myjson.com/bins/2cc7q";
        biedronkas = null;
        getShops();

    }

    public void getShops(){
        new LongRunningGetIO().execute();
    }
    public LatLng getCenter(){
        if(biedronkas!=null)
        {
            LatLng southwest;
            LatLng northeast;
            southwest = new LatLng((double)min_lng ,(double)min_lat );
            northeast = new LatLng((double)max_lng,(double)max_lat);
            LatLngBounds bounds = new LatLngBounds(southwest,northeast);
            return bounds.getCenter();
        }
        return new LatLng(0, 0);
    }
    private class LongRunningGetIO extends AsyncTask <Void, Void, String>
    {
        protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException
        {
            InputStream in = entity.getContent();
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            return total.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            if(biedronkas!=null && mMapa!=null) {
                for (Shop biedronka : biedronkas) {
                    mMapa.addMarker(
                            new MarkerOptions()
                                    .position(biedronka.getPosition())
                                    .title(biedronka.getAddress())
                                    .snippet(biedronka.getHours())
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.biedronka))
                    );
                }
                mMapa.moveCamera(CameraUpdateFactory.newLatLngZoom(getCenter(), 10));
            }
        }
        private void writeToFile(String data) {
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream( cache,false));
                outputStreamWriter.write(data);
                outputStreamWriter.close();
            }
            catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }
        @Override
        protected String doInBackground(Void... params)
        {
            String text = null;

            try
            {
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(surl);
                HttpResponse response = httpClient.execute(httpGet, localContext);
                HttpEntity entity = response.getEntity();
                text = getASCIIContentFromEntity(entity);
                writeToFile(text);
            }
            catch (Exception e)
            {
            }
            try {
                if(text == null)
                    text = readFromFile();
                JSONObject jsonObj;
                jsonObj = new JSONObject(text);
                JSONArray shops = jsonObj.getJSONArray("shops");
                biedronkas = new Shop[shops.length()];
                for (int i = 0; i < shops.length(); i++) {
                    JSONObject c = shops.getJSONObject(i);
                    String id = c.getString("id");
                    String lat = c.getString("lat");
                    String lng = c.getString("lng");
                    String address = c.getString("address");
                    String hours = c.getString("hours");
                    min_lat = Float.parseFloat(lat) < min_lat ? Float.parseFloat(lat) : min_lat;
                    min_lng = Float.parseFloat(lng) < min_lng ? Float.parseFloat(lng) : min_lng;
                    max_lat = Float.parseFloat(lat) > max_lat ? Float.parseFloat(lat) : max_lat;
                    max_lng = Float.parseFloat(lng) > max_lng ? Float.parseFloat(lng) : max_lng;
                    biedronkas[i] = new Shop(Long.parseLong(id), Float.parseFloat(lat), Float.parseFloat(lng), address, hours);
                }
            } catch (Exception e)
            {

            }
            return text;
        }
        private String readFromFile() {

            String ret = "";

            try {
                InputStream inputStream = new FileInputStream(cache);

                if ( inputStream != null ) {
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String receiveString = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    while ( (receiveString = bufferedReader.readLine()) != null ) {
                        stringBuilder.append(receiveString);
                    }

                    inputStream.close();
                    ret = stringBuilder.toString();
                }
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }

            return ret;
        }

    }

}

